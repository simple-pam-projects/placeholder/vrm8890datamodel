package scot.dagda.vrm8890DataModel;

import java.io.Serializable;

public class Event implements Serializable {

    private static final long serialVersionUID = 2332978710002357698L;

    private String eventId;
    private String assessmentId;
    private String batchId;
    private String eventType;


    public Event() {
    }

    public Event(String eventId, String assessmentId, String batchId, String eventType) {
        this.eventId = eventId;
        this.assessmentId = assessmentId;
        this.batchId = batchId;
        this.eventType = eventType;
    }

    public String getEventId() {
        return this.eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getAssessmentId() {
        return this.assessmentId;
    }

    public void setAssessmentId(String assessmentId) {
        this.assessmentId = assessmentId;
    }

    public String getBatchId() {
        return this.batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getEventType() {
        return this.eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public Event eventId(String eventId) {
        this.eventId = eventId;
        return this;
    }

    public Event assessmentId(String assessmentId) {
        this.assessmentId = assessmentId;
        return this;
    }

    public Event batchId(String batchId) {
        this.batchId = batchId;
        return this;
    }

    public Event eventType(String eventType) {
        this.eventType = eventType;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " eventId='" + getEventId() + "'" +
            ", assessmentId='" + getAssessmentId() + "'" +
            ", batchId='" + getBatchId() + "'" +
            ", eventType='" + getEventType() + "'" +
            "}";
    }


}
