package scot.dagda.vrm8890DataModel;

import java.io.Serializable;

public class Parameter implements Serializable {

    private static final long serialVersionUID = 6668083169489364476L;

    private String key;
    private String value;


    public Parameter() {
    }

    public Parameter(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Parameter key(String key) {
        this.key = key;
        return this;
    }

    public Parameter value(String value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " key='" + getKey() + "'" +
            ", value='" + getValue() + "'" +
            "}";
    }


}
