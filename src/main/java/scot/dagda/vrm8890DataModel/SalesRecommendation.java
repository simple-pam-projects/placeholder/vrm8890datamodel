/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scot.dagda.vrm8890DataModel;

/**
 *
 * @author erouvas
 */
public class SalesRecommendation {

  private String vehicleId;
  private String salesPlatform;
  private String salesChannel;

  public SalesRecommendation() {
  }

  public SalesRecommendation(String vehicleId, String salesPlatform, String salesChannel) {
    this.vehicleId = vehicleId;
    this.salesPlatform = salesPlatform;
    this.salesChannel = salesChannel;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public String getSalesPlatform() {
    return salesPlatform;
  }

  public void setSalesPlatform(String salesPlatform) {
    this.salesPlatform = salesPlatform;
  }

  public String getSalesChannel() {
    return salesChannel;
  }

  public void setSalesChannel(String salesChannel) {
    this.salesChannel = salesChannel;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("SalesRecommendation{vehicleId=").append(vehicleId);
    sb.append(", salesPlatform=").append(salesPlatform);
    sb.append(", salesChannel=").append(salesChannel);
    sb.append('}');
    return sb.toString();
  }

  
  
}
