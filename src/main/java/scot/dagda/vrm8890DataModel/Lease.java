/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scot.dagda.vrm8890DataModel;

import java.time.LocalDate;

/**
 *
 * @author erouvas
 */
public class Lease {

  private String vehicleId;
  private LocalDate expiryDate;

  public Lease() {
  }

  public Lease(String vehicleId, LocalDate expiryDate) {
    this.vehicleId = vehicleId;
    this.expiryDate = expiryDate;
  }

  public String getVehicleId() {
    return this.vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public LocalDate getExpiryDate() {
    return this.expiryDate;
  }

  public void setExpiryDate(LocalDate expiryDate) {
    this.expiryDate = expiryDate;
  }

  public Lease vehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
    return this;
  }

  public Lease expiryDate(LocalDate expiryDate) {
    this.expiryDate = expiryDate;
    return this;
  }

  @Override
  public String toString() {
    return "{" +
      " vehicleId='" + getVehicleId() + "'" +
      ", expiryDate='" + getExpiryDate() + "'" +
      "}";
  }

}
