package scot.dagda.vrm8890DataModel;

import java.io.Serializable;

public class Adaptations implements Serializable {

    private static final long serialVersionUID = 3530893786005246869L;

    private String vehicleId;
    private String adaptationId;

    public Adaptations() {
    }

    public Adaptations(String vehicleId, String adaptationId) {
        this.vehicleId = vehicleId;
        this.adaptationId = adaptationId;
    }

    public String getVehicleId() {
        return this.vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getAdaptationId() {
        return this.adaptationId;
    }

    public void setAdaptationId(String adaptationId) {
        this.adaptationId = adaptationId;
    }

    public Adaptations vehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public Adaptations adaptationId(String adaptationId) {
        this.adaptationId = adaptationId;
        return this;
    }

    @Override
    public String toString() {
        return "{" + " vehicleId='" + getVehicleId() + "'" + ", adaptationId='" + getAdaptationId() + "'" + "}";
    }
}