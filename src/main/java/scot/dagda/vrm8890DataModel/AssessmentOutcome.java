package scot.dagda.vrm8890DataModel;

import java.io.Serializable;

public class AssessmentOutcome implements Serializable {

    private static final long serialVersionUID = 2827865351113706846L;

    private String vehicleId;

    // [ ASSIGN, UNASSIGN, UPDATE ]
    private String action;

    // [ MFLDIRECT, AUCTION, REFURB, HUB, NO_PLATFORM ]
    private String platform;

    // PRE_EXPIRY, DFC, ET
    private String channel;


    public AssessmentOutcome() {
    }

    public AssessmentOutcome(String vehicleId, String action, String platform, String channel) {
        this.vehicleId = vehicleId;
        this.action = action;
        this.platform = platform;
        this.channel = channel;
    }

    public String getVehicleId() {
        return this.vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getAction() {
        return this.action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPlatform() {
        return this.platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public AssessmentOutcome vehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public AssessmentOutcome action(String action) {
        this.action = action;
        return this;
    }

    public AssessmentOutcome platform(String platform) {
        this.platform = platform;
        return this;
    }

    public AssessmentOutcome channel(String channel) {
        this.channel = channel;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " vehicleId='" + getVehicleId() + "'" +
            ", action='" + getAction() + "'" +
            ", platform='" + getPlatform() + "'" +
            ", channel='" + getChannel() + "'" +
            "}";
    }


}
