package scot.dagda.vrm8890DataModel;

import java.io.Serializable;

public class VehicleStatus implements Serializable {

    private static final long serialVersionUID = -8859937182462848001L;

    private String vehicleId;
    private String statusAttribute;
    private String statusValue;


    public VehicleStatus() {
    }

    public VehicleStatus(String vehicleId, String statusAttribute, String statusValue) {
        this.vehicleId = vehicleId;
        this.statusAttribute = statusAttribute;
        this.statusValue = statusValue;
    }

    public String getVehicleId() {
        return this.vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getStatusAttribute() {
        return this.statusAttribute;
    }

    public void setStatusAttribute(String statusAttribute) {
        this.statusAttribute = statusAttribute;
    }

    public String getStatusValue() {
        return this.statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    public VehicleStatus vehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
        return this;
    }

    public VehicleStatus statusAttribute(String statusAttribute) {
        this.statusAttribute = statusAttribute;
        return this;
    }

    public VehicleStatus statusValue(String statusValue) {
        this.statusValue = statusValue;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " vehicleId='" + getVehicleId() + "'" +
            ", statusAttribute='" + getStatusAttribute() + "'" +
            ", statusValue='" + getStatusValue() + "'" +
            "}";
    }


}
