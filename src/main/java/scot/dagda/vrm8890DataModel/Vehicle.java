/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scot.dagda.vrm8890DataModel;

import java.io.Serializable;
import java.math.BigDecimal;

// import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * @author erouvas
 */
// @XStreamAlias("Vehicle")
public class Vehicle implements Serializable {

  private static final long serialVersionUID = 3448385785923539021L;

  private String vehicleId;
  private Boolean availableForRemarketing;
  private Boolean hasCherisedPlate;
  private String hasAgreementMechanicalFailure;
  private Integer damagedPanels;
  private boolean hasUnresolvedItem;


  // instead of using the Adaptations object
  private Boolean hasAdaptations;

  // LIVE, PROPOSED, TERMINATED
  private String agreementStatus;

  //
  // If Mileage structure not a list, place attributes in main Vehilce object
  //
  private Boolean hasMileageDescrepancy;
  private BigDecimal projectedMileage;

  //
  // if Profile structure not a list, place attributes in main Vehile object
  //
  private String assetType;

  //
  // if Statuses structure not a list, place attributes in main Vehile object
  //
  // - which "Journey Status" corresponds to that ?
  //
  private Boolean onMfldirect;

  private String plannedHandbackDate;

  //
  //
  // sample attributes - can be discarded
  //
  private String firstRegistrationDate;
  private String resgistration;
  private String capId;
  private String originalVehicleCapId;
  private String originalVehicleType;
  private String condition;


  public Vehicle() {
  }


  public Vehicle(String vehicleId, Boolean availableForRemarketing, Boolean hasCherisedPlate, String hasAgreementMechanicalFailure, Integer damagedPanels, boolean hasUnresolvedItem, Boolean hasAdaptations, String agreementStatus, Boolean hasMileageDescrepancy, BigDecimal projectedMileage, String assetType, Boolean onMfldirect, String plannedHandbackDate, String firstRegistrationDate, String resgistration, String capId, String originalVehicleCapId, String originalVehicleType, String condition) {
    this.vehicleId = vehicleId;
    this.availableForRemarketing = availableForRemarketing;
    this.hasCherisedPlate = hasCherisedPlate;
    this.hasAgreementMechanicalFailure = hasAgreementMechanicalFailure;
    this.damagedPanels = damagedPanels;
    this.hasUnresolvedItem = hasUnresolvedItem;
    this.hasAdaptations = hasAdaptations;
    this.agreementStatus = agreementStatus;
    this.hasMileageDescrepancy = hasMileageDescrepancy;
    this.projectedMileage = projectedMileage;
    this.assetType = assetType;
    this.onMfldirect = onMfldirect;
    this.plannedHandbackDate = plannedHandbackDate;
    this.firstRegistrationDate = firstRegistrationDate;
    this.resgistration = resgistration;
    this.capId = capId;
    this.originalVehicleCapId = originalVehicleCapId;
    this.originalVehicleType = originalVehicleType;
    this.condition = condition;
  }

  public String getVehicleId() {
    return this.vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  public Boolean isAvailableForRemarketing() {
    return this.availableForRemarketing;
  }

  public Boolean getAvailableForRemarketing() {
    return this.availableForRemarketing;
  }

  public void setAvailableForRemarketing(Boolean availableForRemarketing) {
    this.availableForRemarketing = availableForRemarketing;
  }

  public Boolean isHasCherisedPlate() {
    return this.hasCherisedPlate;
  }

  public Boolean getHasCherisedPlate() {
    return this.hasCherisedPlate;
  }

  public void setHasCherisedPlate(Boolean hasCherisedPlate) {
    this.hasCherisedPlate = hasCherisedPlate;
  }

  public String getHasAgreementMechanicalFailure() {
    return this.hasAgreementMechanicalFailure;
  }

  public void setHasAgreementMechanicalFailure(String hasAgreementMechanicalFailure) {
    this.hasAgreementMechanicalFailure = hasAgreementMechanicalFailure;
  }

  public Boolean isHasAdaptations() {
    return this.hasAdaptations;
  }

  public Boolean getHasAdaptations() {
    return this.hasAdaptations;
  }

  public void setHasAdaptations(Boolean hasAdaptations) {
    this.hasAdaptations = hasAdaptations;
  }

  public String getAgreementStatus() {
    return this.agreementStatus;
  }

  public void setAgreementStatus(String agreementStatus) {
    this.agreementStatus = agreementStatus;
  }

  public Boolean isHasMileageDescrepancy() {
    return this.hasMileageDescrepancy;
  }

  public Boolean getHasMileageDescrepancy() {
    return this.hasMileageDescrepancy;
  }

  public void setHasMileageDescrepancy(Boolean hasMileageDescrepancy) {
    this.hasMileageDescrepancy = hasMileageDescrepancy;
  }

  public BigDecimal getProjectedMileage() {
    return this.projectedMileage;
  }

  public void setProjectedMileage(BigDecimal projectedMileage) {
    this.projectedMileage = projectedMileage;
  }

  public String getAssetType() {
    return this.assetType;
  }

  public void setAssetType(String assetType) {
    this.assetType = assetType;
  }

  public Boolean isOnMfldirect() {
    return this.onMfldirect;
  }

  public Boolean getOnMfldirect() {
    return this.onMfldirect;
  }

  public void setOnMfldirect(Boolean onMfldirect) {
    this.onMfldirect = onMfldirect;
  }

  public String getPlannedHandbackDate() {
    return this.plannedHandbackDate;
  }

  public void setPlannedHandbackDate(String plannedHandbackDate) {
    this.plannedHandbackDate = plannedHandbackDate;
  }

  public String getFirstRegistrationDate() {
    return this.firstRegistrationDate;
  }

  public void setFirstRegistrationDate(String firstRegistrationDate) {
    this.firstRegistrationDate = firstRegistrationDate;
  }

  public String getResgistration() {
    return this.resgistration;
  }

  public void setResgistration(String resgistration) {
    this.resgistration = resgistration;
  }

  public String getCapId() {
    return this.capId;
  }

  public void setCapId(String capId) {
    this.capId = capId;
  }

  public String getOriginalVehicleCapId() {
    return this.originalVehicleCapId;
  }

  public void setOriginalVehicleCapId(String originalVehicleCapId) {
    this.originalVehicleCapId = originalVehicleCapId;
  }

  public String getOriginalVehicleType() {
    return this.originalVehicleType;
  }

  public void setOriginalVehicleType(String originalVehicleType) {
    this.originalVehicleType = originalVehicleType;
  }

  public String getCondition() {
    return this.condition;
  }

  public void setCondition(String condition) {
    this.condition = condition;
  }

  public Vehicle vehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
    return this;
  }

  public Vehicle availableForRemarketing(Boolean availableForRemarketing) {
    this.availableForRemarketing = availableForRemarketing;
    return this;
  }

  public Vehicle hasCherisedPlate(Boolean hasCherisedPlate) {
    this.hasCherisedPlate = hasCherisedPlate;
    return this;
  }

  public Vehicle hasAgreementMechanicalFailure(String hasAgreementMechanicalFailure) {
    this.hasAgreementMechanicalFailure = hasAgreementMechanicalFailure;
    return this;
  }

  public Vehicle hasAdaptations(Boolean hasAdaptations) {
    this.hasAdaptations = hasAdaptations;
    return this;
  }

  public Vehicle agreementStatus(String agreementStatus) {
    this.agreementStatus = agreementStatus;
    return this;
  }

  public Vehicle hasMileageDescrepancy(Boolean hasMileageDescrepancy) {
    this.hasMileageDescrepancy = hasMileageDescrepancy;
    return this;
  }

  public Vehicle projectedMileage(BigDecimal projectedMileage) {
    this.projectedMileage = projectedMileage;
    return this;
  }

  public Vehicle assetType(String assetType) {
    this.assetType = assetType;
    return this;
  }

  public Vehicle onMfldirect(Boolean onMfldirect) {
    this.onMfldirect = onMfldirect;
    return this;
  }

  public Vehicle plannedHandbackDate(String plannedHandbackDate) {
    this.plannedHandbackDate = plannedHandbackDate;
    return this;
  }

  public Vehicle firstRegistrationDate(String firstRegistrationDate) {
    this.firstRegistrationDate = firstRegistrationDate;
    return this;
  }

  public Vehicle resgistration(String resgistration) {
    this.resgistration = resgistration;
    return this;
  }

  public Vehicle capId(String capId) {
    this.capId = capId;
    return this;
  }

  public Vehicle originalVehicleCapId(String originalVehicleCapId) {
    this.originalVehicleCapId = originalVehicleCapId;
    return this;
  }

  public Vehicle originalVehicleType(String originalVehicleType) {
    this.originalVehicleType = originalVehicleType;
    return this;
  }

  public Vehicle condition(String condition) {
    this.condition = condition;
    return this;
  }

  public Integer getDamagedPanels() {
    return this.damagedPanels;
  }

  public void setDamagedPanels(Integer damagedPanels) {
    this.damagedPanels = damagedPanels;
  }

  public boolean isHasUnresolvedItem() {
    return this.hasUnresolvedItem;
  }

  public boolean getHasUnresolvedItem() {
    return this.hasUnresolvedItem;
  }

  public void setHasUnresolvedItem(boolean hasUnresolvedItem) {
    this.hasUnresolvedItem = hasUnresolvedItem;
  }


  @Override
  public String toString() {
    return "{" +
      " vehicleId='" + getVehicleId() + "'" +
      ", availableForRemarketing='" + isAvailableForRemarketing() + "'" +
      ", hasCherisedPlate='" + isHasCherisedPlate() + "'" +
      ", hasAgreementMechanicalFailure='" + getHasAgreementMechanicalFailure() + "'" +
      ", damagedPanels='" + getDamagedPanels() + "'" +
      ", hasUnresolvedItem='" + isHasUnresolvedItem() + "'" +
      ", hasAdaptations='" + isHasAdaptations() + "'" +
      ", agreementStatus='" + getAgreementStatus() + "'" +
      ", hasMileageDescrepancy='" + isHasMileageDescrepancy() + "'" +
      ", projectedMileage='" + getProjectedMileage() + "'" +
      ", assetType='" + getAssetType() + "'" +
      ", onMfldirect='" + isOnMfldirect() + "'" +
      ", plannedHandbackDate='" + getPlannedHandbackDate() + "'" +
      ", firstRegistrationDate='" + getFirstRegistrationDate() + "'" +
      ", resgistration='" + getResgistration() + "'" +
      ", capId='" + getCapId() + "'" +
      ", originalVehicleCapId='" + getOriginalVehicleCapId() + "'" +
      ", originalVehicleType='" + getOriginalVehicleType() + "'" +
      ", condition='" + getCondition() + "'" +
      "}";
  }


}
