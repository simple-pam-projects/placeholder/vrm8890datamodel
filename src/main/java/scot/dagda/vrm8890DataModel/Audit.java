/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scot.dagda.vrm8890DataModel;

/**
 *
 * @author erouvas
 */
public class Audit {
  
  private String vehicleId;
  private String ruleName;

  public Audit() {
  }

  public Audit(String vehicleId, String ruleName) {
    this.vehicleId = vehicleId;
    this.ruleName = ruleName;
  }

  public String getRuleName() {
    return ruleName;
  }

  public void setRuleName(String ruleName) {
    this.ruleName = ruleName;
  }

  public String getVehicleId() {
    return vehicleId;
  }

  public void setVehicleId(String vehicleId) {
    this.vehicleId = vehicleId;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("Audit{vehicleId=").append(vehicleId);
    sb.append(", ruleName=").append(ruleName);
    sb.append('}');
    return sb.toString();
  }
  
  
  
}
